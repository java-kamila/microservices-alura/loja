# API Loja (Floricultura)

## Estrutura

<img src="spring-micro-servicos-alura.png" alt="Estrutura dos Microservices">

 - Teremos três microsserviços: Fornecedor, Loja e Transportador

 - Uma apresentação da modelagem focado em DDD (Domain Driven Design)
     - Quebraremos o domínio em contextos menores (bounded context)
     - Um microsserviço é a implementação de um contexto

### Endpoints

<img src="endpoints_services.png" alt="Estrutura dos Microservices">

### Comunicação

<img src="eureka.png" alt="Estrutura dos Microservices">

### Anotações

 - A dependência `Eureka Discovery Client` permite a inscrição da API como um cliente em um servidor Eureka
 - As configurações em `application.properties` 
 	- Registrando a API como cliente Eureka:
 
 ```
	eureka.client.register-with-eureka=true
	eureka.client.fetch-registry=true
	## IP do servidor Eureka
	eureka.client.service-url.defaultZone=http://localhost:8761/eureka
 ```

 	- Registrando o nome da API:
 
	```
	spring.application.name=loja
	```
##### Balanceamento de carga - Client-Side Load-Balancing

<img src="load-balance.png" alt="Estrutura com vários fornecedores">

 - Temos três tecnologias principais nesse cenário de **Load Balancing**: O `RestTemplate`, que usamos para fazer uma requisição a uma aplicação, através do seu nome, o `Eureka Client`, que fornece as instâncias disponíveis de um determinado serviço, e o `Ribbon` que, através da anotação `@LoadBalanced` (**Loja**), aprimora o RestTemplate com o processo de Load Balancing.
 - Em Loja Server o Balanceamento de carga através de [Client-Side Load-Balancing](https://spring.io/guides/gs/spring-cloud-loadbalancer/) realiza a escolha do ip:porta do Fornecedor caso existam várias instâncias de Fornecedor na estrutura.
 - `Load Balancing` é o processo de distribuir as requisições vindas dos usuários para as várias instâncias disponíveis.
 
##### Requisições


 - POST:
 	 - http://localhost:8080/compra
 	 ```json
 	 // body
 	 {
	    "itens": [
	        {
	            "id": 1,
	            "quantidade": 6
	        }
	    ],
	    "endereco": {
	        "rua": "Rua da Maria",
	        "numero": 101,
	        "estado": DF
	    }
	}
 
 - GET:
 	- http://localhost:8080/compra/{pedidoId}
 	
 	
##### Distributed Tracing - Logg
 - Rastreamento distribuído de loggs
 - Assim como nos sistemas monolíticos, temos logs separados em máquinas diferentes, mas, apenas nos microsserviços, a lógica de negócio também está quebrada em logs diferentes
 - Uma requisição do usuário bate em várias aplicações diferentes, para que a lógica de negócio requerida seja realizada. Com isso, acompanhar os logs gerados em uma transação não é tão simples quanto abrir um único log e ter toda a informação disponível.
 - Para unificar os logs, precisamos de agregadores de log
	- Como implementação de um agregador, usamos o [PaperTrail](https://papertrailapp.com/), um agregador como serviço. Necessária criação de conta e "add system". Mantém os logs armazenados por 7 dias.
 - Usamos a biblioteca Logback para gerar e enviar os logs ao agregador
 - O Logback possui um appender, que possibilita o envio dos logs
 - Para acompanhar uma transação nos logs, usamos uma correlation-id
 - A correltation-id é um identificador da transação, que é passada de requisição pra requisição. Dessa forma, podemos entender quais requisições fazem parte da mesma transação
 - Seguindo [documentação](https://help.papertrailapp.com/kb/configuration/java-logback-logging/#syslogappender) criamos o arquivo `logback.xml` na pasta _src/main/resources_ de Fornecedor e Loja para que os logs gerados por estes systemas sejam armazenados e acessados em um único local, no PaperTrail.
 
##### Spring Sleuth
 - Bliblioteca usada para gerar a correlation-id. 
 - Seguindo [documentação](https://docs.spring.io/spring-cloud-sleuth/docs/2.2.4.RELEASE/reference/html/#json-logback-with-logstash) do [Spring Sleuth](https://docs.spring.io/spring-cloud-sleuth/docs/2.2.4.RELEASE/reference/html/) adicionada linha: `<include resource="org/springframework/boot/logging/logback/defaults.xml"/>` em logback.xml
 - Alterando `<suffixPattern>` em logback.xml para o value descrito em:
 ```xml
 <property name="CONSOLE_LOG_PATTERN"
              value="%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(${LOG_LEVEL_PATTERN:-%5p}) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n${LOG_EXCEPTION_CONVERSION_WORD:-%wEx}"/>
 ```
 
##### Tratando Erros

<img src="tratando-erros.png" />
 
 - Loja Server controla o status da compra e retorna para o cliente.
 - Este cliente refere-se a uma outra funcionalidade que tenha a lógica de orquestração da transação de compra. Teríamos então o CompraController chamando o `OrquestradorDeCompraService`, que chamaria o CompraService. De acordo com o estado da compra retornada pelo CompraService, o orquestrador decidiria reprocessar, cancelar, etc.
 - A cada nova transação o status da compra é atualizado e persistido em banco de dados
 - Caso aconteça alguma exceção o método `Fallback` é chamado
 
##### Autenticação

 - Para habilitar a autenticação do usuário foi adicionada a anotation `@EnableResourceServer` em *LojaApplication* e criada a classe `ResourceServerConfigurer`que estende de `ResourceServerConfigurerAdapter`, onde são sinalizados os recursos e e configurações de autenticação.
 - A aplicação passa a buscar informações do client que está tentando acessar os recursos na API Authentication através do url indicado em `application.properties`:
 `security.oauth2.resource.user-info-uri=http://localhost:8088/user`
 
###### Repassando token (Feign interceptor)

 - Inserir configuração de segurança do hystrix em application.properties para habilitar captura de informações de segurança da thread que acessou o recurso.
 `hystrix.shareSecurityContext: true` 
 - Inserir na classe principal a interceptação das requsições para captura do token:
 ```java
 	@Bean
	public RequestInterceptor getInterceptorAutenticacao() {
		return new RequestInterceptor() {
			
			@Override
			public void apply(RequestTemplate template) {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				if (authentication == null) {
					return;
				}
				
				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails)authentication.getDetails();
				template.header("Authorization", "Bearer" + details.getTokenValue());
			}
		};
	}
```
 