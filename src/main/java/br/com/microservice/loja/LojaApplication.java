package br.com.microservice.loja;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.client.RestTemplate;

import feign.RequestInterceptor;
import feign.RequestTemplate;

@SpringBootApplication
@EnableFeignClients
@EnableCircuitBreaker
@EnableScheduling
@EnableResourceServer
public class LojaApplication {
	
	/*
	 * 	@Bean - Indica que Spring gerancia RestTemplate. Disponível para injeção.
	 *  @LoadBalanced - Permite a descoberta do ip e porta pelo nome de aplicação inscrita no Eureka Server.
	 *  	Fornece o balanceamento de carga do lado do cliente em chamadas para outro microsserviço.
	 *  	Existindo vários fornecedores o @LoadBalance faz esse balanceamento de carga.
	 *  	Exemplo de acesso em CompraService, método realizaCompra.
	 */
	
	/*
	 *  Chamado a cada requisição do Feign
	 *  Insere token recebido nas requisições para outros microservices
	 */
	@Bean
	public RequestInterceptor getInterceptorAutenticacao() {
		return new RequestInterceptor() {
			
			@Override
			public void apply(RequestTemplate template) {
				Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
				if (authentication == null) {
					return;
				}
				
				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails)authentication.getDetails();
				template.header("Authorization", "Bearer" + details.getTokenValue());
			}
		};
	}
	
	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	public static void main(String[] args) {
		SpringApplication.run(LojaApplication.class, args);
	}

}
